/*global angular*/
'use strict';

angular.
    module('bitcraft-rest-http').
    factory('RestHttp', [
        '$http', '$q',
        function ($http, $q) {
            /**
             * Perform a get request on the REST api
             * @param address {string} address of the listening api.
             * @param [params] {Object} params passed to the api.
             * @param [options] {Object} options passed to angular http service.
             */
            function restGet(address, params, options) {
                var deferred = $q.defer();

                if (!options) {
                    options = {};
                }

                if (params) {
                    options.params = params;
                }

                $http.get('./rest/' + address, options).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Perform a post request on the REST api
             * @param address {string} address of the listening api.
             * @param data {Object} data passed to the api.
             * @param [options] {Object} data passed to the api.
             */
            function restPost(address, data, options) {
                var deferred = $q.defer();

                $http.post('./rest/' + address, data, options).then(function successCallback(resp) {
                    deferred.resolve(resp.data);
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            return {
                restGet: restGet,
                restPost: restPost
            };
        }
    ]);
