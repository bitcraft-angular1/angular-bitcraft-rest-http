(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
require('./restHttp.module');
require('./restHttp.service');

},{"./restHttp.module":2,"./restHttp.service":3}],2:[function(require,module,exports){
/*global angular*/
angular.module('bitcraft-rest-http', []);

},{}],3:[function(require,module,exports){
/*global angular*/
'use strict';

angular.
    module('bitcraft-rest-http').
    factory('RestHttp', [
        '$http', '$q',
        function ($http, $q) {
            /**
             * Perform a get request on the REST api
             * @param address {string} address of the listening api.
             * @param [params] {Object} params passed to the api.
             * @param [options] {Object} options passed to angular http service.
             */
            function restGet(address, params, options) {
                var deferred = $q.defer();

                if (!options) {
                    options = {};
                }

                if (params) {
                    options.params = params;
                }

                $http.get('./rest/' + address, options).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Perform a post request on the REST api
             * @param address {string} address of the listening api.
             * @param data {Object} data passed to the api.
             * @param [options] {Object} data passed to the api.
             */
            function restPost(address, data, options) {
                var deferred = $q.defer();

                $http.post('./rest/' + address, data, options).then(function successCallback(resp) {
                    deferred.resolve(resp.data);
                }, function errorCallback(resp) {
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            return {
                restGet: restGet,
                restPost: restPost
            };
        }
    ]);

},{}]},{},[1]);
